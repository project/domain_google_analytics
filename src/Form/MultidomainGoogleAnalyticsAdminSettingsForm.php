<?php

namespace Drupal\multidomain_google_analytics\Form;

use Drupal\Core\Link;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;

/**
 * Configure Google_Analytics settings for this site.
 */
class MultidomainGoogleAnalyticsAdminSettingsForm extends ConfigFormBase {

  /**
   * The config object for the multidomain_google_analytics settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct function.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entity_type_manager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create function return static domain loader configuration.
   *
   * @param Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Load the ContainerInterface.
   *
   * @return \static
   *   return domain loader configuration.
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'), $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multidomain_google_analytics_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['multidomain_google_analytics.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domains = $this->entityTypeManager->getStorage('domain')->loadMultiple();
    if ($domains) {
      $form['general'] = [
        '#type' => 'details',
        '#title' => $this->t('Multidomain Google Analytics Settings'),
        '#open' => TRUE,
      ];
      $configFactory = $this->config('multidomain_google_analytics.settings');

      foreach ($domains as $domain) {
        $hostname = $domain->gethostname();
        if ($domain->id()) {
          $form['general'][$domain->id()] = [
            '#type' => 'textfield',
            '#title' => $this->t('Google Analytics ID for Domain: @hostname', ['@hostname' => $hostname]),
            '#description' => $this->t('The ID assigned by Google Analytics for this website container.'),
            '#maxlength' => 64,
            '#size' => 64,
            '#default_value' => $configFactory->get($domain->id()),
            '#weight' => '0',
          ];
        }
      }
      return parent::buildForm($form, $form_state);
    }
    else {
      $url = Url::fromRoute('domain.admin');
      $domain_link = Link::fromTextAndUrl($this->t('Domain records'), $url);
      $form['title']['#markup'] = $this->t('There is no Domain record yet.Please create a domain records.See link: @domain_list', ['@domain_list' => $domain_link]);
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('multidomain_google_analytics.settings');
    $domains = $this->entityTypeManager->getStorage('domain')->loadMultiple();
    foreach ($domains as $key => $value) {
      $config->set($key, $form_state->getValue($key))->save();
    }

    parent::submitForm($form, $form_state);
  }

}
