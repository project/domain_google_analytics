CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Adds the Google Analytics tracking system to your multidomain website.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/domain_google_analytics

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/domain_google_analytics


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Domain Access - https://www.drupal.org/project/domain

This module also requires to have Google Analytics account:

 * Google Analytics user account -
   https://marketingplatform.google.com/about/analytics


INSTALLATION
------------

 * Install the Multidomain Google Analytics module as you would normally install
   a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > System > Multidomain Google
       Analytics for configuration.

You can checkout Multidomain Google Analytics code at any page by using view
source.


MAINTAINERS
-----------

 * Sushil Pal (sushilpal) - https://www.drupal.org/u/sushilpal

Supporting organizations:

 * TO THE NEW - https://www.drupal.org/to-the-new
